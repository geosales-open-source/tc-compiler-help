package com.totalcross.util.compile;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopiedAutodeleteJar implements Closeable {
	private final Path createdJar;

	public CopiedAutodeleteJar(String original, String destiny) throws IOException {
		Path originalPath = Paths.get(original);
		createdJar = Paths.get(destiny);
		Files.copy(originalPath, createdJar);
	}

	@Override
	public void close() throws IOException {
		Files.delete(createdJar);
	}
}
