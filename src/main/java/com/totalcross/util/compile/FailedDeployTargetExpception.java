package com.totalcross.util.compile;

import java.util.HashSet;
import java.util.Set;

public class FailedDeployTargetExpception extends RuntimeException {

	private static final long serialVersionUID = -5704331566843282383L;
	private final Set<String> failedTargets;

	public FailedDeployTargetExpception(String failedTarget) {
		super("Couldn't generate target " + failedTarget);
		this.failedTargets = new HashSet<>();
		failedTargets.add(failedTarget);
	}

	public FailedDeployTargetExpception(Set<String> failedTargets) {
		super("Some targets have errors: " + failedTargets);
		this.failedTargets = new HashSet<>(failedTargets);
	}

	public Set<String> getFailedTargets() {
		return failedTargets;
	}
}
