# tc-compiler-help
Uma maneira mais fácil de se fazer o build do TotalCross usando Maven.

Tenha em mente que, além da dependência da API do TotalCross
`com.totalcross:totalcross-sdk`, é necessário tê-lo instalado na máquina para
se ter acesso aos esqueletos de executáveis e também a outros detalhes mais.

Esse projeto é um _fork_ do
[`com.totalcross.utils:tc-compiler-help`](https://github.com/TotalCross/tc-compiler-help)
