# 2.2.0

Focada em tentar ser uma versão ainda mais leve, removendo ainda mais plugins e dependências desnecessárias.
Também entrou nas diretrizes virar distribuição basicamente JitPack.

Outros pontos que entraram foi usar mais ferramentas Java para resolver os problemas enfrentados pelo
`tc-compiler-help`, como copiar arquivos. Soluções Java (como `java.nio.Path` e `java.nio.Files`) foram
usadas no lugar, até porque com a `nio` eu tenho acesso a eventuais otimizações dependentes de sistema que
seriam implementadas pelas abstrações na JVM.

Também subi uma solução que era feita constantemente a nível de projeto de app TotalCross, que é a criação
do `jar` temporário com o nome da classe principal. Não está intrínseco ao ciclo de vida do `CompilationBuilder`,
mas já fornece um alívio.

Disponível pelo JitPack como `com.gitlab.geosales-open-source:tc-compiler-help:2.2.0`

## Funcionalidades

- classe `CopiedAutodeleteJar` permite criar um jar com o nome da classe principal auto-deletável
  - essa era uma solução comum implementada por quem importava essa dependência, porém aqui eu posso usar
    classes e métodos que ofendem o subconjunto da JDK no TotalCross

## Mudanças

- ciclo de distribuição via JitPack
- tentando tornar a dependência ainda mais leve
  - creio que haja espaço removendo dependências transitivas do TotalCross, visto que a nível de geração
    do artefato `tc-compiler-help` só me interessa a API pública do TotalCross

# 2.1.0

Focada em melhorar disponibilidade para _smoke tests_.

Disponível pelo repositório Maven da TotalCross como `br.com.softsite:tc-compiler-help:2.1.0`

Disponível pelo JitPack como `com.gitlab.geosales-open-source:tc-compiler-help:v2.1.0`

## Funcionalidades

- lança exceção quando há falha no processo de `tc.Deploy`, seja do artefato principal ou das dependências
  - sim, saída do programa diferente de 0, pode usar no CI
  - possibilidade de explodir logo no começo (padrão) ou deixar acumular e exibir todas as explosões `CompilationBuilder.setEarlyExplode(false)`
- possibilidade de fazer um _dry run_ do jar do app e de suas dependências, para assim fazer o _smoke test_ da compilação
  - caso seja encontrado falha em alguma dependência, se impede de gerar o executável final "setando" essa opção como verdade no moemnto da falha
- permitindo usar outro `targetBaseDir` com `CompilationBuilder.setTargetBaseDir(directory)`
  - essa opção foi criada para permitir adaptar, a partir de um reator Maven, a chamada para a compilação com TotalCross
  - o `targetBaseDir` padrão continua sendo `target`

## Mudanças

- o `CompilationBuilder` termina com falha (lançando exceção) para indicar que não foi possível gerar o executável
  - a exceção lançada é `com.totalcross.util.compile.FailedDeployTargetExpception`, você pode investigar os artefatos falhos
    com `FailedDeployTargetException.getFailedTargets()`
- o comportamento padrão é abortar na primeira falha

# 2.0.0

Fork do projeto [`com.totalcross:tc-compiler-help`](https://github.com/TotalCross/tc-compiler-help)

## Funcionalidades

- `CompilationBuilder.getenv(String)` para pegar valores de variáveis de ambiente sem ofender
  ao TotalCross
- fazendo a compilação do `tcz` estilo Makefile, mais ou menos como se tivesse a regra `%.tcz : %.jar`
  para as dependências

## Correções

- endereço do repositório Maven do TotalCross corrigido para usar o DNS

## Mudanças

- remoção de plugins desnecessários
- dependência do TotalCross agora com o escopo `provided`
  - a versão do TotalCross que trata corretamente do split das dependências é `>= 4.1.5`
- imprimindo a linha de comando passada ao TotalCross na saída padrão. Procure por `cmd line:`
  seguido de um `List`
- sempre usar `try-with-resources` quando lidando com `totalcross.io.File`
  - abdicando do `ACFile` porque o `totalcross.io.File` já é `Closeable`